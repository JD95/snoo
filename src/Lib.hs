{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MultiWayIf #-}

module Lib where

import Data.List
import GHC.Generics
import Data.Aeson
import Data.Aeson.Casing
import Control.Lens
import Data.Aeson.Lens 
import Network.Wreq
import Network.HTTP.Types.Status
import qualified Data.Text as T
import qualified Data.ByteString.Lazy as BS
import Data.Text hiding (tail, zipWith)
import Network.Wai (pathInfo, responseLBS, Application)
import Network.Wai.Handler.Warp (run)
import Control.Concurrent
import Control.Concurrent.MVar
import System.Process

data OAuth
  = OAuth
  { oauthClientId :: Text
  , oauthResponseType :: Text
  , oauthState :: Text
  , oauthRedirectUri :: Text
  , oauthDuration :: Text
  , oauthScope :: Text
  } deriving (Generic)

instance ToJSON OAuth where
  toJSON = genericToJSON (aesonPrefix snakeCase)

instance FromJSON OAuth where
  parseJSON = genericParseJSON (aesonPrefix snakeCase)

oauthParams o = defaults 
  & param "client_id" .~ [oauthClientId o]
  & param "response_type" .~ [oauthResponseType o]
  & param "state" .~ [oauthState o]
  & param "redirect_uri" .~ [oauthRedirectUri o]
  & param "duration" .~ [oauthDuration o]
  & param "scope" .~ [oauthScope o]

data TokenRetrieval
  = TokenRetrieval
  { tokenretrievalError :: Text
  , tokenretrievalCode :: Text
  , tokenretrievalState :: Text
  } deriving (Generic)

instance ToJSON TokenRetrieval where
  toJSON = genericToJSON (aesonPrefix snakeCase)

instance FromJSON TokenRetrieval where
  parseJSON = genericParseJSON (aesonPrefix snakeCase)

someFunc :: IO ()
someFunc = putStrLn "someFunc"

loginServer mLoginPage mToken req out =
  if | pathInfo req == ["login"] -> do
         putStrLn "Login Request"
         page <- readMVar mLoginPage
         out $ responseLBS status200 [] page 
     | pathInfo req == ["auth"] -> do
         putStrLn "Login Successful"
         putMVar mToken (show req)
         out $ responseLBS status200 [] ""
     | otherwise -> out $ responseLBS status404 [] ""

getOAuthToken request = do
  mToken <- newEmptyMVar 
  mLoginPage <- newEmptyMVar
  receiver <- forkIO $ run 4040 (loginServer mLoginPage mToken)
  r <- getWith (oauthParams request) "https://www.reddit.com/api/v1/authorize"
  putMVar mLoginPage $ r ^. responseBody
  _ <- system "vivaldi http://127.0.0.1:4040/login" 
  print =<< readMVar  mToken
  killThread receiver
